"""
 Bits: Simple constants that make bit-setting a little more readable.
 Provides the developer a quicker way to understand just what is being set to
 a register.

 To make full bit masks, OR, invert, and AND the constants together.
 e.g. 0xB4 = 0b10110100 = BIT_7 | BIT_5 | BIT_4 | BIT_2
"""

NULL = 0x00
BIT_0 = 0x01
BIT_1 = 0x02
BIT_2 = 0x04
BIT_3 = 0x08
BIT_4 = 0x10
BIT_5 = 0x20
BIT_6 = 0x40
BIT_7 = 0x80
