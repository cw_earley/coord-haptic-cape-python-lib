#!/usr/bin/env python
"""
PCA9685 Lib

 This library aims to be a generic python interface to the NXP PCA9685
 16-channel, I2 bit PWM C-bus LED controller for the BeagleBone series of
 development platforms.

 By default this library does nothing upon instantiation outside of creating
 an Adafruit_I2C instance, provide a bunch of readable constants, and
 provide a selection of useful functions.

 Setting up the chip to meet your needs will need to be handled in your code
 like so:
 # Create new PCA9685 obj at the specified I2C address, default is 0x40
 >>> p = PCA9685(I2C_ADDR=0xXX)
 # Turn on auto incrementing registers and LED all call
 >>> p.write_register(p.MODE1_ADDR, p.MODE1_AUTOINCREMENT | p.MODE1_LEDALLCALL)
 # Make outputs change on ACK and setup outputs in totem-pole structure
 >>> p.write_register(p.MODE2_ADDR, p.MODE2_OCH | p.MODE2_OUTDRV)

 Assumptions:
    * The PCA9685 is connected to the BB via I2C2

Notes:
    * There are some fancy things to be done with MODE2_OUTNE[0:1] with regard
    to shutting off all inputs just by pulling a pin high/low. Read the datasheet
    for more info and know that you'll have to handle the OE pin yourself.
    * Using your own clock signal for PWM is possible but pay attention to funcs
    like set_frequency().
    * reset() is to be used during operation. shutdown() is to be used as the last
    command sent to the chip before finishing service. Do not try to reset() a
    chip that has been shutdown(). It'll throw an error!

 For more information on the PCA9685 please see the official data sheet at:
 http://bit.ly/1Rq125R

 History
 -----------------------
 Author        Date            Comments
 Chris Earley  3-18-2016       Initial Authoring
"""

from Adafruit_I2C import Adafruit_I2C
# Useful, readable values for making bitmasks
from bits_lib import *
import math
import time

# Gotta get that new class style
# https://docs.python.org/2/reference/datamodel.html#new-style-and-classic-classes
class PCA9685(object):
    """
    PCA9685 encasulates a series of constants as defined in the PCA9685 datasheet
    and provides a set of functions to easily control the chip over I2C
    """

    # constants

    # Starting LED PWM address.
    # Loading a 16 bit (0-4095) PWM time value involves sending 2
    # 8 bit values.
    # To get to other LEDs add (LED_NUM * 4) to these registers
    LED0_ON_L_ADDR = 0x06
    LED0_ON_H_ADDR = 0x07
    LED0_OFF_L_ADDR = 0x08
    LED0_OFF_H_ADDR = 0x09

    # ALL LED registers. For controlling all the outputs at once
    ALL_LED_ON_L_ADDR = 0xFA
    ALL_LED_ON_H_ADDR = 0xFB
    ALL_LED_OFF_L_ADDR = 0xFC
    ALL_LED_OFF_H_ADDR = 0xFD

    # Setting this to the MSB ON/OFF register for an LED will make
    # that output 100% ON/OFF.
    LED_H_FULL = BIT_4

    # Mode1 Register Address
    MODE1_ADDR = 0x00
    # Mode1 Register Bits
    MODE1_RESTART = BIT_7
    MODE1_ECTCLK = BIT_6
    MODE1_AUTOINCREMENT = BIT_5
    MODE1_SLEEP = BIT_4
    MODE1_SUB1 = BIT_3
    MODE1_SUB2 = BIT_2
    MODE1_SUB3 = BIT_1
    MODE1_LEDALLCALL = BIT_0

    # Mode2 Register Address
    MODE2_ADDR = 0x01
    # Mode2 Register Bits
    # Bits 7-5 are reserved for future use by NXP
    MODE2_INVRT = BIT_4
    MODE2_OCH = BIT_3
    MODE2_OUTDRV = BIT_2
    MODE2_OUTNE1 = BIT_1
    MODE2_OUTNE0 = BIT_0

    # PWM output frequency
    PRE_SCALE_ADDR = 0xFE

    # All LED output value for setPWM
    ALL_LED = 16
    
    # Clamp values for active outputs
    maxOutput=4095
    minOutput=0

    def __init__(self, I2C_ADDR=0x40, debug=False, maxOutput=4095, minOutput=0):
        """
        Construct a new PCA9685 object.
        :param I2C_ADDR: The I2C address of the chip. This is specified via the
            [A0:A5] pins on the chip itself. Low=0, High=1
        :param debug: Whether or not debug statements should be printed to the
            console during code execution.
        """
        # Be default the I2C addr is 0x40, meaning A0-A5 are all 0
        self.I2C_ADDR = I2C_ADDR
        # debug mode, for printing out extra information and performing sanity
        # checks
        self.debug = debug
        
        # the maximum and minimum output levels
        self.maxOutput = maxOutput
        self.minOutput = minOutput

        # I should be able to push the debug flag along but it causes
        # Adafruit_I2C to fail for some reason. I'll triage it later...
        self.i2c = Adafruit_I2C(self.I2C_ADDR)

    def set_frequency(self, freq, OSC_CLK=25000000.0):
        """
        Update the chip's PWM frequency. This assumes you are using the internal
        oscillator clocked in a 25 MHz

        :param freq: The PWM frequency to set. [24:1526] Hz
        :returns: returns nothing
        """
        freq = freq * 0.99  # Super weird hack to fix the equation. See http://bit.ly/1PjXP09
        dividend = OSC_CLK  # defaults to the internal CLK @ 25 * 10**6, 25 MHz
        divisor = 4096.0 * float(freq)
        prescale_value = (dividend / divisor) - 1.0  # Equation 1
        prescale_value = int(math.floor(prescale_value))
        if self.debug:
            print("Prescale value:" + str(prescale_value) + ", Hex: " + str(hex(prescale_value)))

        # store the current mode1 so we can revert
        prevModeOne = self.read_register(self.MODE1_ADDR)
        # nap time~
        self.write_register_OR(self.MODE1_ADDR, self.MODE1_SLEEP)
        # Time to set the freq!
        self.write_register(self.PRE_SCALE_ADDR, prescale_value)
        # restore the old mode settigns
        self.write_register(self.MODE1_ADDR, prevModeOne)
        # wait ~5 microseconds for the oscilator to recover
        time.sleep(0.0005)  # 500.0 * 10 ** -6, 500 microseconds

    def restart(self):
        """
        Reset the chip as defined in the datasheet with checking.
        Works only when the chip is not sleeping!

        If you shutdown() properly beforehand this will throw an error as it
        isn't needed!
        """
        # store the current mode1 so we can revert
        prevModeOne = self.read_register(self.MODE1_ADDR)
        # set the restart bit
        self.write_register(self.MODE1_ADDR, prevModeOne | self.MODE1_SLEEP)

        currModeOne = self.read_register(self.MODE1_ADDR)
        # if the restart bit is set
        if currModeOne & self.MODE1_RESTART:
            # clear the sleep bit
            self.write_register(self.MODE1_ADDR, currModeOne & ~self.MODE1_SLEEP)
        else:
            print("restart(): Error setting restart bit!")
            return -1
        # finally restart the chip
        self.write_register_OR(self.MODE1_ADDR, BIT_7)
        # wait ~5 microseconds for the oscilator to recover
        time.sleep(0.0005)  # 500.0 * 10 ** -6, 500 microseconds
        if self.debug:
            print("After delay, reset bit is: " + str(self.read_register(self.MODE1_ADDR) >> 7) + ". Should be 0!")
        # restore the original MODE1 settings
        self.write_register(self.MODE1_ADDR, prevModeOne)

    def shutdown(self):
        """
        Perform an "orderly shutdown".
        Turning off all of the outputs by using BIT_4 on ALL_LED_ON
        """
        # "write a logic 1 to bit 4 in register ALL_LED_OFF_H"
        self.setDuty(self.ALL_LED, 0)
        # put the chip into sleep mode
        self.write_register_OR(self.MODE1_ADDR, self.MODE1_SLEEP)

    def setDuty(self, outputNum, duty, inv=False, AI=False):
        """
        Update the given output's PWM without needing to manually compute
        the on/off times.

        :param outputNum: The output [0:16] to update. 16 is the ALL_LED register.
        :param duty: The duty cycle of the output. Range is [0:4095]
        :param inv: If the output logic state is inverted (MODE2_INVRT)
            set to true, else false
        :param AI: Whether auto increment is available or not. (MODE1_AUTOINCREMENT)
        If it's set we can use writeList instead of manually writing the values.
        :returns: returns nothing
        """
        on, off = self.checkPWMDuty(duty, inv)
        if AI:
            self.setPWM_list(outputNum, on, off)
        else:
            self.setPWM(outputNum, on, off)

    def setPWM(self, outputNum, on, off, inv=False):
        """
        Update the given output's values using 4 8bit data transmissions.
        Use this when MODE1_AUTOINCREMENT isn't set.

        :param outputNum: The output [0:16] to update. 16 is the ALL_LED register.
        :param on: The time the output state will be asserted [0:4095]
        :param off: The time the output state will be negated [0:4095]
        :param inv: If the output logic state is inverted (MODE2_INVRT)
            set to true, else false
        :returns: returns nothing
        """
        if outputNum == 16:
            # if the all LED value is given, change the offset to equal 250 for
            # the first write
            offset = self.ALL_LED_ON_L_ADDR - self.LED0_ON_L_ADDR
        else:
            # Since it takes 4 register updates to set an LED, we can skip to any
            # LED register just by adding the addr for LED0 to the
            # LED number we want times 4.
            offset = outputNum * 4
        self.i2c.write8(self.LED0_ON_L_ADDR + offset, on)
        self.i2c.write8(self.LED0_ON_H_ADDR + offset, on >> 8)
        self.i2c.write8(self.LED0_OFF_L_ADDR + offset, off)
        self.i2c.write8(self.LED0_OFF_H_ADDR + offset, off >> 8)

    def setPWM_list(self, outputNum, on, off):
        """
        Update the given output's values using 1 data transmission
        the MODE1_AUTOINCREMENT bit will need to be set for this.

        :param outputNum: The output [0:16] to update. 16 is the ALL_LED register.
        :param on: The time the output state will be asserted [0:4095]
        :param off: The time the output state will be negated [0:4095]
        :param inv: If the output logic state is inverted (MODE2_INVRT)
            set to true, else false
        :returns: returns nothing
        """
        if outputNum == 16:
            # if the all LED value is given, change the offset to equal 250 for
            # the first write
            offset = self.ALL_LED_ON_L_ADDR - self.LED0_ON_L_ADDR
        else:
            # Since it takes 4 register updates to set an LED, we can skip to any
            # LED register just by adding the addr for LED0 to the
            # LED number we want times 4.
            offset = outputNum * 4
        l = [on, on >> 8, off, off >> 8]
        self.i2c.writeList(self.LED0_ON_L_ADDR + offset, l)

    def checkPWMDuty(self, duty, inv=False):
        """
        Perform a small sanity check on the ON time, correct for inverted output
        logic, and set the LED_H_FULL bit for special cases.

        If on/off is set to 0 or 4095, the opposite value is zeroed out.
        check_PWM_values(4095) -> (LED_H_FULL << 8, 0)
        check_PWM_values(0) ->  (0, LED_H_FULL << 8)

        :param on: The time the output state will be asserted [0:4095]
        :param inv: If the output logic state is inverted (MODE2_INVRT)
            set to True, else False
        :returns: returns a tuple containing the new on/off values: (on, off)
        """
        # clamp to inclusive range
        duty = self.restrict(duty, self.minOutput, self.maxOutput)

        if self.debug:
            print("checkPWMDuty(): Clamped duty value: " + str(duty))

        # For special cases where full ON or OFF is desired for an output
        # also if on/off is Max/Min just zero out the other register.
        if inv:
            # if any of the values are of extremes
            if duty == 4095:
                off = self.LED_H_FULL << 8
                on = 0x00
            elif duty == 0:
                on = self.LED_H_FULL << 8
                off = 0x00
            # Invert the values for the normal case
            else:
                on = 0x00
                off = 4095 - duty
        else:
            # if any of the values are of extremes
            if duty == 4095:
                on = self.LED_H_FULL << 8
                off = 0x00
            elif duty == 0:
                off = self.LED_H_FULL << 8
                on = 0x00
            else:
                on = 0x00
                off = duty
        if self.debug:
            print("checkPWMDuty(): Final PWM values: (" + str(on) + "," + str(off) + ")")
        return (on, off)

    def restrict(self, val, minval, maxval):
        """
        Restrict the active outputs to between two values. Useful for compensating
        for motor stall conditions or limiting harsher vibrations.
        Any value lower than minval will get dropped to 0/OFF
        Any value higher than maxval will get clamped to maxval.

        :param val: Value to clamp between an inclusive limit.
        :param minval: Minimum possible active value to be sent to the output
        :param maxval: Maximum possible value to be sent to the output
        :returns: The clamped value
        """
        if val < minval:
            return 0
        if val > maxval:
            return maxval
        return val

    def write_register(self, register, value):
        """
        Write an 8 bit value to a given register

        :param register: The register to write to
        :param value: The value to write
        :returns: returns nothing
        """
        if self.debug:
            print("write_register(): Writing " + str(hex(value)) + " to " + str(hex(register)))
        self.i2c.write8(register, value)

    def write_register_OR(self, register, value):
        """
        Binary OR the existing register value with the given value and update.
        Good for small updates when you just want to flip one bit.

        :param register: The register to update
        :param value: The value to write
        :returns: returns nothing
        """
        old_register = self.read_register(register)
        if self.debug:
            print("write_register_OR(): Writing " + str(hex(value)) + "|" +
                  str(hex(old_register)) + " to " + str(hex(register)))
        self.i2c.write8(register, self.read_register(register) | value)

    def write_register_AND(self, register, value):
        """
        Binary AND the existing register value with the given value and update.
        Good for small updates when you just want to flip one bit.

        :param register: The register to update
        :param value: The value to write. Binary NOT (~) is useful here!
        :returns: returns nothing
        """
        old_register = self.read_register(register)
        if self.debug:
            print("write_register_AND(): Writing " + str(hex(value)) + "&" +
                  str(hex(old_register)) + " to " + str(hex(register)))
        self.i2c.write8(register, self.read_register(register) & value)

    def read_register(self, addr):
        """
        Read data from a register.

        :param register: The register to read
        :returns: The register's value
        """
        return self.i2c.readU8(addr)
