#!/usr/bin/env python
"""
Just a small demo that changes output on the two side tactors [2 & 6]
in response to a user's tilt. When the user sways to one side, that tactor will
begin to vibrate in warning with an intensity that increases the more they lean.

NOTE:
You will need to mount the compass sensor flush to the user's stomach for this
to work since we can't rely on the z-axis to calculate tilt without an accelerometer.

This demo assumes you possess:
    * Grove 3-Axis Digital Compass
    * BeagleBone Haptic Cape
    * An 8-tactor haptic belt
"""
import sys
from haptic_cape_lib import Haptic
from grove_compass_lib import compass
import time

# whether or not the sensor orientation is reversed
# if the tactors seem to be moving against your movements, set this to True
reverse = False

def main():
    # Start up all our hardware
    h = Haptic(maxOutput=3000)
    c = compass()
    
    # The maximum angle the user can sway to one side. Used for scaling the haptic
    # feedback
    swayRange = 30
    # Our desired vibration level
    vibLevel = 3000
    
    print("Calculating normal standing angle. Please stand still ...")
    # for the first few seconds while the user is standing up straight,
    # take sensor readings to get an average value for the desired angle
    # and calculate the cumulative moving average
    prevAverage = 0
    for n in range(1, 30):
        c.update()
        sample = c.headingDegrees
        if reverse:
            sample = 360 - sample
        averageAngle = (sample + (n - 1) * prevAverage) / n
        prevAverage = averageAngle
        time.sleep(0.2)
    print("Normal Angle: " + str(round(averageAngle, 2)))
    
    while True:
        # Update the Grove Compass sensor
        c.update()
        # grab the user's current tilt
        currentAngle = c.headingDegrees
        # correct for weird sensor placements
        if reverse:
            currentAngle = 360 - currentAngle
        # calculate the difference between the desired standing angle and the 
        # current. Since the angle increases clockwise, we can get an idea of
        # which side the user is leaning by looking at the sign.
        swayAngle = averageAngle - currentAngle
        # For each side, use the calculated angle and the sway range to 
        # scale the vibration level. Don't worry about sending vibration
        # values larger than 4095, the lower level libs will clamp the value.
        if swayAngle < 0:
            rightHaptic = abs(swayAngle) * vibLevel / swayRange
            leftHaptic = 0
        else:
            leftHaptic = abs(swayAngle) * vibLevel / swayRange
            rightHaptic = 0
        # Create a Dictionary containing 2 key:value pairs in the form of
        # {OutputNumber: OutputValue}
        d = {2: int(leftHaptic), 6: int(rightHaptic)}
        # Update the Haptic Cape outputs
        h.setDuty_dict(d)
        # Nap for a little bit.
        time.sleep(0.04)
if __name__ == "__main__":
    main()
