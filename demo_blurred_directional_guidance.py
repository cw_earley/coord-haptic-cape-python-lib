#!/usr/bin/env python
from haptic_cape_lib import Haptic
from grove_compass_lib import compass
from time import sleep
import math

"""
A more comprehensive demo of haptic guidance that takes into account the
recommendations of Nagel SK et al. in their paper
"Beyond sensory substitution--learning the sixth sense."

The application will change output on the first 8 [0:7] output channels
in response to rotation of a Seeed Grove Compass Sensor using blurring
techniques to create a more organic transition between state updates.

NOTE:
You will need to mount the compass sensor with the PCB's bottom facing the
ground in order to calculate a correct heading.

This demo assumes you possess:
    * Grove 3-Axis Digital Compass
    * BeagleBone Haptic Cape
    * An 8-tactor haptic belt

"""

# Our maximum desired vibration value
maxValue = 3000
# degree offsets to compensate for odd sensor placement
offset = 0
# whether or not the sensor orientation needs to be reversed
# if the tactors seem to be rotating against your movements, set this to True
reverse = False

def main():
    # Start up all our hardware
    h = Haptic(maxOutput=4095)
    c = compass()
    # precompute the gaussian curve that we'll use for blurring outputs
    # values between [3:8] work best for 8-tactor belts
    blurArray = precompute_gauss(200, 5)

    while True:
        # Update the Compass
        c.update()
        # find the output index that's nearest to North, return a float
        direction = ((c.headingDegrees + offset) / 45.0) % 8
        if reverse:
           direction = 360 - direction
        # Use that index to generate a blurred output state
        d = generate_blur(direction, 8, blurArray)
        # update the haptic cape outputs
        h.setDuty_dict(d)
        # Nap for a little while
        sleep(0.1)


def precompute_gauss(arrayLength, blurWidth):
    """
    Compute an array of values from [0:1] by which to scale the belt outputs.
    This will be used to create an organic, bluring effect where one output's state
    will seamlessly blend into the states of nearby outputs as the user rotates.
    The scaling values follow a simple gaussian curve.

    Since this gaussian distribution is symmetrical, we only need to calculate
    one half of it. Woo hoo!

    :param length: The number of gauss samples that should be generated.
    :param width: The amount of "blurring" to apply. Larger amounts mean that
    when one output is 100% on, neighboring outputs will also vibrate.
    Think about how blurring works for photos!
    :returns : An array of len(length/2) of floating point scaling values.
    """
    result = []
    for i in range(arrayLength / 2):
        dividend = math.pow(i, 2)
        divisor = 2 * math.pow(blurWidth, 2)
        # accounting for the case where blur == 0, or no blur
        if divisor == 0:
            # when x -> infinity, e^x = 0
            if i == 0:
                result.append(1)
            else:
                result.append(0)
        else:
            # python floats can be hairy so lets simplity our results
            # based from example on http://stackoverflow.com/a/455634
            result.append(round(math.exp(-1 * dividend / divisor), 3))
    return result


def generate_blur(sensorDir, numOutputs, distribution):
    global maxValue
    """
    This is the function that takes in the current direction, the precomputed
    scaling distribution and the number of tactors used to generate a haptic
    state where the tactor closest to North will buzz but that buzzing will
    bleed into neighboring tactors in a seamless manner.

    :param sensorDir: The direction the sensor is reporting in the range of [0:numOutputs]
    :param numOutputs: The number of haptic outputs the system uses.
    :param distribution: The precomputed array of scaling values.
    :returns : A dictionary containing numOutputs scaled {outputNumber: outputValue} pairs.
    """
    scaledOutputs = {}
    for i in range(numOutputs):
        # calculate the linear distance between the current index and the
        # index facing north and scale it with respect to the total number of outputs
        # so we get a value from [0:1]
        relativeDistance = closest_circular_distance(sensorDir, i, numOutputs) / float(numOutputs)
        # Using that percentage, we can seek out the precomputed gaussian value
        # and scale our max output value by it!
        scaledValue = scale_output(relativeDistance, maxValue, distribution)
        # If the value is super low, just clamp it.
        # Worst case for power draw is motor stall
        if scaledValue < 300:
            scaledValue = 0
        scaledOutputs[i] = int(scaledValue)
    return scaledOutputs


def closest_circular_distance(a, b, numOutputs):
    """
    Since a belt is best conceptualized as a continuous circle, we need to be able
    to find the closest distance between two point on that circle.

    :param a: The first point on the circle
    :param b: The second point on the circle
    :param numOutputs: The number of haptic outputs the system uses
    :returns : A float value of the distance between the given points
    """
    rawDistance = abs(a - b)
    limitedDistance = rawDistance % numOutputs
    # if the current raw distance is longer than half the ring's circumference
    # it must be the longer distance!
    if limitedDistance > numOutputs / 2.0:
        return numOutputs - limitedDistance
    else:
        return limitedDistance


def scale_output(relativeDistance, maxValue, distribution):
    """
    Using the percentage distance value, find the accompanying value in the
    scaling array and scale the max output value by that amount.
    :param relativeDistance: The distance between two point on the belt,
    represented as a percentage value between [0,1]
    :param maxValue: The maximum output value for this output. Usually 4095.
    :param distribution: The precomputed scaling distribution
    :returns : A integer scaled output value.
    """
    i = int(len(distribution) * relativeDistance)
    return maxValue * distribution[i]


if __name__ == '__main__':
    main()
