# Haptic Cape Python Library for BeagleBone Black/Green by Coord.Space

This repository is meant to be a companion to the BeagleBone Haptic Cape development platform.

For more information about the project and how to setup the hardware for the demos, please see the Hackster.IO [project documentation](https://www.hackster.io/cw-earley/haptic-neurohacking-101-08ac18).

### Library
* haptic_cape_lib.py

  This file provides an easy to use interface for the Beaglebone Haptic Cape with a focus on controlling Eccentric Rotating Mass (ERM) motors. Read the Included pydoc page for more information on what functions are available for controlling the cape.

### Demonstrations
Currently the repository contains four demos. Each requiring the user to have an 8-Tactor haptic belt wearable as described in the hacker.io project docs here: https://www.hackster.io/cw-earley/haptic-neurohacking-101-08ac18

* demo_haptic_pulse.py

  This program simply pulses each of the cape's 16 outputs in sequence 10 times before shutting down.
  This is best used to test communication between the BeagleBone and the Cape or to test that all of the Cape's outputs are in working order.

* demo_simple_direction_guidance.py

  Just a small demo that changes output on the first 8 [0:7] output channels in response to rotation of a Seeed Grove 3-Axis Compass Sensor.
  Is a simplistic emulation of the FeelSpace Haptic Belt product.

* demo_blurred_directional_guidance.py

  A more advanced guidance demo that builds upon the simple version to add a semi-gaussian blur factor to the haptic display. This makes presenting the compass data to the user feel just a little more natural.

* demo_simple_balance.py

  Just a small script similar to the simple guidance demo that changes output on the side output channels of the Cape [2 & 6] in response to a user's tilt / sway.

## Special Thanks To
Seeed Studio for the [Grove 3-Axis Digital Compass](http://www.seeedstudio.com/depot/Grove-3Axis-Digital-Compass-p-759.html) library.

## Contributing

Feel free to fork, pull, suggest improvements, and report any bugs.
If you require any help getting the system installed and working, feel free to message me via
bitbucket mail.

## License

These files have been made available online through a [Creative Commons Attribution-ShareAlike 3.0](http://creativecommons.org/licenses/by-sa/3.0/) license.
