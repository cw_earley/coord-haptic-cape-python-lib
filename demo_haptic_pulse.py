from haptic_cape_lib import Haptic
import time

"""
Just a simple helper script for testing that all outputs work.
Pulses all outputs one after another in a loop 10 times then shuts
the cape down safely before terminating.

"""


def main():
    # The level of vibration
    vibLevel = 2000
    # Start up the Cape
    print("Starting Cape...")
    h = Haptic()
    print("Cape started!")
    # generic incrementer variable
    k = 0
    while k < 10:
        for j in range(16):
            d = {i: (vibLevel if i == j else 0) for i in range(16)}
            h.setDuty_dict(d)
            time.sleep(0.2)
        print("Test cycle " + str(k) + " finished")
        k += 1
    print("Test cycle done! Shutting cape down...")
    h.shutdown()
    print("Cape is now off!")

if __name__ == "__main__":
    main()
