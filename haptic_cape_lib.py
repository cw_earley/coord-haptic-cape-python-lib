#!/usr/bin/env python
"""
Haptic Cape Lib

 This is the wrapper library that provides an easy to use interface for the
 Beaglebone Haptic Cape with a focus on controlling Eccentric Rotating Mass (ERM) motors.

 History
 -----------------------
 Author        Date            Comments
 Chris Earley  3-18-2016       Initial Authoring

 More more information on the PCA9685 please see the offical data sheet at:
 http://bit.ly/1Rq125R
"""


import PCA9685_lib
import Adafruit_BBIO.GPIO as GPIO
import time


class Haptic(PCA9685_lib.PCA9685):

    OE_PIN = "P9_23"

    def __init__(self, *args, **kwargs):
        """
        Construct a new Haptic object.
        :param I2C_ADDR: The I2C address of the chip. This is specified via the
            [A0:A5] pins on the chip itself. Low=0, High=1
        :param debug: Whether or not debug statements should be printed to the
            console during code execution.
        """
        super(Haptic, self).__init__(*args, **kwargs)

        # enable register auto incrementing and the ability to call all LEDS
        self.write_register(self.MODE1_ADDR, self.MODE1_AUTOINCREMENT | self.MODE1_LEDALLCALL)
        # update on ACK and we're using OUTDRV mode for this board
        self.write_register(self.MODE2_ADDR, self.MODE2_OCH | self.MODE2_OUTDRV)
        # set PWM freq to 250 so we get a quieter spin
        self.set_frequency(250)
        # turn all outputs off till we need them.
        self.setDuty(self.ALL_LED, 0)
        # enable the output enable pin and set it high so output can start
        GPIO.setup(self.OE_PIN, GPIO.OUT)
        self.enableOutput()

    def setDuty_dict(self, d):
        """
        Given a dictionary containing n output ports between [0:16] and n
        duty values between [0:4095], update all n PWM values.

        Note: Output 16 is the All_LED port. Meaning anything you set there will
        be applied to all outputs on the board!

        :param list: The dictionary of {outputNum: duty} pairs.
        :returns nothing:
        """
        for item in d.keys():
            self.setDuty(item, d[item])

    def disableOutput(self):
        """
        Disables all output without stopping PWM/Register updates.

        This is done by raising the EO pin on P9.23 to VDD
        """
        GPIO.output(self.OE_PIN, GPIO.HIGH)

    def enableOutput(self):
        """
        Enable all output without stopping PWM/Register updates.

        This is done by grounding the EO pin on P9.23
        """
        GPIO.output(self.OE_PIN, GPIO.LOW)

    def shutdown(self):
        """
        Perform an "orderly shutdown" and clean up after GPIO.
        """
        super(Haptic, self).shutdown()
        GPIO.cleanup()
