#!/usr/bin/env python
"""
Just a small demo that changes output on the first 8 [0:7] output channels
in response to rotation of a Seeed Grove Compass Sensor

This demo assumes you possess:
    * Grove 3-Axis Digital Compass
    * BeagleBone Haptic Cape
    * An 8-tactor haptic belt

"""
import sys
from haptic_cape_lib import Haptic
from grove_compass_lib import compass
import time

# degree offsets to compensate for odd sensor placement
offset = 0
# whether or not the sensor orientation needs to be reversed
# if the tactors seem to be rotating against your movements, set this to True
reverse = False

def main():
    # Start up all our hardware
    h = Haptic()
    c = compass()

    # The level of vibration for the tactor facing north
    vibLevel = 2000

    while True:
        # convert the degrees value from [0:360] to [0:8]
        direction = ((c.headingDegrees + offset) / 45.0) % 8
        if reverse:
           direction = 360 - direction
        # Create a Dictionary containing 8 key:value parts in the form of
        # {OutputNumber: OutputValue}
        # For this case, set the outputNum that faces North to vibLevel
        # and the rest to 0
        d = {i: (vibLevel if i == int(direction) else 0) for i in range(8)}
        # Update the Haptic Cape outputs
        h.setDuty_dict(d)
        # Update the Grove Compass sensor
        c.update()
        # Nap for a little bit.
        time.sleep(0.08)

if __name__ == "__main__":
    main()
